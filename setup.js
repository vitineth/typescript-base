const readline = require('readline');
const fs = require('fs/promises');
const { spawn } = require('child_process');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const asyncQuestion = (question) => new Promise((resolve) => rl.question(question, resolve));

async function setup(){
  const name = await asyncQuestion('name: ');
  const description = await asyncQuestion('description: ');

  let file;
  try{
    file = await fs.readFile('package.json', {encoding: 'utf8'});
  }catch(e){
    console.error('Failed to load the package.json file!', e);
    return;
  }

  let json;
  try{
    json = JSON.parse(file);
  } catch (e) {
    console.error('Failed to parse the package.json file!', e);
    return;
  }

  json.name = name;
  json.description = description;
  
  const output = JSON.stringify(json, undefined, 4);

  console.log('* writing new package.json');

  try{
    await fs.writeFile('package.json', output, {encoding: 'utf8'});
  } catch (e) {
    console.error('Failed to write out the new package.json file');
    return;
  }

  console.log('* making folder structure');

  try{
    await fs.mkdir('src');
    await fs.mkdir('build');
  } catch (e) {
    console.error('Failed to make src/ and build/', e);
    return;
  }

  console.log('* installing packages');

  const cmd = spawn(process.platform === "win32" ? "npm.cmd" : "npm", ['install']);
  cmd.stdout.on('data', process.stdout.write);
  cmd.stderr.on('data', process.stderr.write);
  cmd.on('exit', (code) => {
    console.log('\n* NPM install completed with code: ' + code);
  });
}

void setup();